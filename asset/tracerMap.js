let traceCarte = (codeLigne) => {
    fetch(`http://data.metromobilite.fr/api/lines/json?types=ligne&codes=${codeLigne}`)
    .then((Response)=> Response.json())
    .then((json)=> {

        liste = json.features[0].properties.ZONES_ARRET
        let jsonColor = json.features[0].properties.COULEUR
    
        let myStyle = {
            "color": `rgb(${jsonColor})`
        };

        L.geoJSON(json, {style: myStyle, onEachFeature: function (feature,layer){layer.codeLayer=codeLigne}}).addTo(mymap); 
      
    })
}


let traceCarteRemove = (codeLigne) => {
  
    mymap.removeLayer(layers1[codeLigne])
    mymap.eachLayer(function(layer){
        if (layer.codeLayer && layer.codeLayer === codeLigne) {
            mymap.removeLayer(layer)
        }
    })

}